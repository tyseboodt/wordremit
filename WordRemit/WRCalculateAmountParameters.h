//
//  WRCalculateAmountParameters.h
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <Mantle/Mantle.h>

/**
 *  Used to map the JSON sent to the calculate api.
 */
@interface WRCalculateAmountParameters : MTLModel<MTLJSONSerializing>

@property (readonly, copy, nonatomic) NSString *sendCurrency;
@property (readonly, copy, nonatomic) NSString *receiveCurrency;
@property (readonly, assign, nonatomic) NSNumber *amount;

- (instancetype)initWithAmount:(NSNumber *)amount
                  sendCurrency:(NSString *)sendCurrency
               receiveCurrency:(NSString *)receiveCurrency;


@end
