//
//  WRCalculateAmountParameters.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import "WRCalculateAmountParameters.h"

@interface WRCalculateAmountParameters ()

@property (readwrite, copy, nonatomic) NSString *sendCurrency;
@property (readwrite, copy, nonatomic) NSString *receiveCurrency;
@property (readwrite, assign, nonatomic) NSNumber *amount;

@end

@implementation WRCalculateAmountParameters

- (instancetype)initWithAmount:(NSNumber *)amount
                  sendCurrency:(NSString *)sendCurrency
               receiveCurrency:(NSString *)receiveCurrency
{
    self = [super init];
    if (self != nil) {
        _amount = amount;
        _sendCurrency = sendCurrency;
        _receiveCurrency = receiveCurrency;
    }
    return self;
}

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
                @"amount":@"amount",
                @"sendCurrency":@"sendcurrency",
                @"receiveCurrency":@"receivecurrency"
             };
}

@end
