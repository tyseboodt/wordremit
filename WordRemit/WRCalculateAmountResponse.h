//
//  WRCalculateAmountModel.h
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <Mantle/Mantle.h>

/**
 *  Used to map the JSON returned by the calculate api.
 */
@interface WRCalculateAmountResponse : MTLModel<MTLJSONSerializing>

@property (readonly, copy, nonatomic) NSString *sendCurrency;
@property (readonly, copy, nonatomic) NSString *receiveCurrency;
@property (readonly,assign, nonatomic) NSNumber *sendAmount;
@property (readonly,assign, nonatomic) NSNumber *receiveAmount;

@end
