//
//  WRCalculateAmountModel.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import "WRCalculateAmountResponse.h"

@implementation WRCalculateAmountResponse

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"sendAmount":@"sendamount",
             @"receiveAmount":@"receiveamount",
             @"sendCurrency":@"sendcurrency",
             @"receiveCurrency":@"receivecurrency"
             };
}

@end
