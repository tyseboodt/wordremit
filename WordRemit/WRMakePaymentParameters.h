//
//  WRMakePaymentParameters.h
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <Mantle/Mantle.h>

/**
 *  Used to map the JSON sent to the pay api.
 */
@interface WRMakePaymentParameters : MTLModel<MTLJSONSerializing>

@property (readonly, copy, nonatomic) NSString *sendCurrency;
@property (readonly, copy, nonatomic) NSString *receiveCurrency;
@property (readonly, assign, nonatomic) NSNumber *sendAmount;
@property (readonly, assign, nonatomic) NSNumber *receiveAmount;
@property (readonly, copy, nonatomic) NSString *recipient;

- (instancetype)initWithSendAmount:(NSNumber *)sendAmount
                      sendCurrency:(NSString *)sendCurrency
                     receiveAmount:(NSNumber *)receiveAmount
                   receiveCurrency:(NSString *)receiveCurrency
                         recipient:(NSString *)recipient;


@end
