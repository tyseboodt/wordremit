//
//  WRMakePaymentParameters.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import "WRMakePaymentParameters.h"

@interface WRMakePaymentParameters ()

@property (readwrite, copy, nonatomic) NSString *sendCurrency;
@property (readwrite, copy, nonatomic) NSString *receiveCurrency;
@property (readwrite, assign, nonatomic) NSNumber *sendAmount;
@property (readwrite, assign, nonatomic) NSNumber *receiveAmount;
@property (readwrite, copy, nonatomic) NSString *recipient;

@end

@implementation WRMakePaymentParameters

- (instancetype)initWithSendAmount:(NSNumber *)sendAmount
                  sendCurrency:(NSString *)sendCurrency
                     receiveAmount:(NSNumber *)receiveAmount
               receiveCurrency:(NSString *)receiveCurrency
                         recipient:(NSString *)recipient
{
    self = [super init];
    if (self != nil) {
        _sendAmount = sendAmount;
        _receiveAmount = receiveAmount;
        _sendCurrency = sendCurrency;
        _receiveCurrency = receiveCurrency;
        _recipient = recipient;
    }
    return self;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"sendAmount":@"sendamount",
             @"receiveAmount":@"receiveamount",
             @"sendCurrency":@"sendcurrency",
             @"receiveCurrency":@"receivecurrency",
             @"recipient":@"recipient"
             };
}

@end
