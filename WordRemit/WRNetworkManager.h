//
//  WRNetworkManager.h
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WRCalculateAmountParameters;
@class WRCalculateAmountResponse;
@class WRMakePaymentParameters;

/**
 *  Manager class for all network operations.
 */
@interface WRNetworkManager : NSObject

/**
 *  Fetches the available currencies.
 *
 *  @param completion A completion handler that takes an array of currencies as parameter, or nil if an error occured.
 */
+ (void)currencies:(void (^)(NSArray *))completion;

/**
 *  Calculates the amount the recipient will receive in the receiving currency.
 *
 *  @param parameters The parameters for the request.
 *  @param completion A completion handler that takes a WRCalculateAmountResponse as parameter, or nil if an error occured.
 */
+ (void)calculateAmount:(WRCalculateAmountParameters *)parameters completionBlock:(void (^)(WRCalculateAmountResponse *))completion;

/**
 *  Make a payment to the recipient.
 *
 *  @param parameters The parameters for the payment
 *  @param completion A completion handler that takes a boolean as parameter, indicating wether the payment was successful or not.
 */
+ (void)pay:(WRMakePaymentParameters *)parameters completionBlock:(void (^)(BOOL))completion;

@end
