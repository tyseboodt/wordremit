//
//  WRNetworkManager.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import "WRNetworkManager.h"
#import "WRCalculateAmountParameters.h"
#import "WRCalculateAmountResponse.h"
#import "WRMakePaymentParameters.h"
#import <AFNetworking.h>

@implementation WRNetworkManager

+ (void)currencies:(void (^)(NSArray *))completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"https://wr-interview.herokuapp.com/api/currencies" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completion(nil);
    }];
}

+ (void)calculateAmount:(WRCalculateAmountParameters *)parameters completionBlock:(void (^)(WRCalculateAmountResponse *))completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *requestParameters = [MTLJSONAdapter JSONDictionaryFromModel:parameters error:nil];
    
    [manager GET:@"https://wr-interview.herokuapp.com/api/calculate" parameters:requestParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        WRCalculateAmountResponse *response = nil;
        if (responseObject != nil) {
            response = [MTLJSONAdapter modelOfClass:[WRCalculateAmountResponse class] fromJSONDictionary:responseObject error:nil];
        }
        completion(response);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completion(nil);
    }];
}

+ (void)pay:(WRMakePaymentParameters *)parameters completionBlock:(void (^)(BOOL))completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *requestParameters = [MTLJSONAdapter JSONDictionaryFromModel:parameters error:nil];
    
    [manager POST:@"https://wr-interview.herokuapp.com/api/send" parameters:requestParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([operation.response statusCode] == 201);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completion(NO);
    }];
}

@end
