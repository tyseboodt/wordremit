//
//  WRPaymentCompleteViewController.h
//  WordRemit
//
//  Created by Thierry Yseboodt on 21/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  View controller displayed when the payment was successful.
 */
@interface WRPaymentCompleteViewController : UIViewController

@end
