//
//  WRPaymentCompleteViewController.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 21/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import "WRPaymentCompleteViewController.h"

@implementation WRPaymentCompleteViewController

- (void)viewDidLoad
{
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = NSLocalizedString(@"Congratulations", nil);
}

- (IBAction)homeAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


@end
