//
//  WRPaymentViewController.h
//  WordRemit
//
//  Created by Thierry Yseboodt on 21/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  View controller used to select an amount and make a payment.
 */
@interface WRPaymentViewController : UIViewController <UITextFieldDelegate>

/**
 *  Pass the recipient name for the payment to the view controller.
 *
 *  @param recipientName The recipient name for the payment.
 */
- (void)injectRecipientName:(NSString *)recipientName;

@end
