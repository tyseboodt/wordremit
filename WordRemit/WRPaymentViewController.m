//
//  WRPaymentViewController.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 21/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import "WRPaymentViewController.h"
#import "WRNetworkManager.h"
#import "WRCalculateAmountParameters.h"
#import "WRCalculateAmountResponse.h"
#import "WRMakePaymentParameters.h"

typedef NS_ENUM(NSUInteger, WR_POPUP_TYPE) {
    POPUP_TYPE_CURRENCIES_ERROR,
    POPUP_TYPE_CALCULATE_ERROR,
    POPUP_TYPE_PAY_ERROR,
};

static int kDefaultAmount = 10;

@interface WRPaymentViewController ()

@property (strong, nonatomic) NSArray *currencies;
@property (strong, nonatomic) IBOutlet UILabel *sendCurrencyLabel;
@property (strong, nonatomic) IBOutlet UITextField *sendAmountTextfield;
@property (strong, nonatomic) IBOutlet UILabel *receiveCurrencyLabel;
@property (strong, nonatomic) IBOutlet UILabel *receiveAmountLabel;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (copy, nonatomic) NSString *recipientName;

@end

@implementation WRPaymentViewController

- (void)viewDidLoad
{
    self.sendAmountTextfield.text = [NSString stringWithFormat:@"%d", kDefaultAmount];
    self.navigationItem.title = NSLocalizedString(@"Payment", nil);
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil
                                                                         action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done", nil)
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(dismissTextField)]];
    [numberToolbar sizeToFit];
    self.sendAmountTextfield.inputAccessoryView = numberToolbar;
    [self fetchCurrencies];
}

- (void)fetchCurrencies
{
    __weak WRPaymentViewController *weakSelf = self;
    [WRNetworkManager currencies:^(NSArray *currencies) {
        WRPaymentViewController *strongSelf = weakSelf;
        if (strongSelf) {
            if (currencies != nil && currencies.count >= 2) {
                strongSelf.currencies = currencies;
                [strongSelf updateCurrencies];
                [strongSelf updateReceivedAmount];
            } else {
                UIAlertView *currenciesErrorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", nil)
                                                                               message:NSLocalizedString(@"There was an error contacting our servers.", nil)
                                                                              delegate:strongSelf
                                                                     cancelButtonTitle:NSLocalizedString(@"Retry", nil)
                                                                     otherButtonTitles:nil];
                currenciesErrorAlert.tag = POPUP_TYPE_CURRENCIES_ERROR;
                [currenciesErrorAlert show];
            }
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = [NSString stringWithFormat:NSLocalizedString(@"Pay %@", nil), self.recipientName];
}

- (void)injectRecipientName:(NSString *)recipientName
{
    self.recipientName = recipientName;
}

- (void)dismissTextField
{
    [self.sendAmountTextfield resignFirstResponder];
}

- (void)updateCurrencies
{
    self.sendCurrencyLabel.text = self.currencies[0];
    self.receiveCurrencyLabel.text = self.currencies[1];
}

- (void)updateReceivedAmount
{
    self.sendButton.userInteractionEnabled = NO;
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    WRCalculateAmountParameters *params = [[WRCalculateAmountParameters alloc] initWithAmount:[numberFormatter numberFromString:self.sendAmountTextfield.text]
                                                                                 sendCurrency:self.sendCurrencyLabel.text
                                                                              receiveCurrency:self.receiveCurrencyLabel.text];
    __weak WRPaymentViewController *weakSelf = self;
    [WRNetworkManager calculateAmount:params
                      completionBlock:^(WRCalculateAmountResponse *response) {
                          WRPaymentViewController *strongSelf = weakSelf;
                          if (strongSelf) {
                              if (response != nil) {
                                  strongSelf.receiveAmountLabel.text = [response.receiveAmount stringValue];
                                  strongSelf.sendButton.userInteractionEnabled = YES;
                              } else {
                                  UIAlertView *calculateErrorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", nil)
                                                                                                message:NSLocalizedString(@"There was an error calculating the received amount. Please review the amount.", nil)
                                                                                               delegate:strongSelf
                                                                                      cancelButtonTitle:NSLocalizedString(@"Ok", nil)
                                                                                      otherButtonTitles:nil];
                                  calculateErrorAlert.tag = POPUP_TYPE_CALCULATE_ERROR;
                                  [calculateErrorAlert show];
                              }
                          }
                      }];
}

- (IBAction)payAction:(id)sender
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    WRMakePaymentParameters *payParams = [[WRMakePaymentParameters alloc] initWithSendAmount:[numberFormatter numberFromString:self.sendAmountTextfield.text]
                                                                                sendCurrency:self.sendCurrencyLabel.text
                                                                               receiveAmount:[numberFormatter numberFromString:self.receiveCurrencyLabel.text]
                                                                             receiveCurrency:self.receiveCurrencyLabel.text
                                                                                   recipient:self.recipientName];
    __weak WRPaymentViewController *weakSelf = self;
    [WRNetworkManager pay:payParams
          completionBlock:^(BOOL succeeded) {
              WRPaymentViewController *strongSelf = weakSelf;
              if (strongSelf) {
                  if (succeeded) {
                      [strongSelf performSegueWithIdentifier:@"WRPaymentCompleteViewController" sender:strongSelf];
                  } else {
                      UIAlertView *paymentErrorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", nil)
                                                                                  message:NSLocalizedString(@"We couldn't proceed with the payment. Please review details and try again", nil)
                                                                                 delegate:strongSelf
                                                                        cancelButtonTitle:NSLocalizedString(@"Ok", nil)
                                                                        otherButtonTitles:nil];
                      paymentErrorAlert.tag = POPUP_TYPE_PAY_ERROR;
                      [paymentErrorAlert show];
                      
                  }
              }
          }];
}

#pragma mark UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateReceivedAmount];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case POPUP_TYPE_CALCULATE_ERROR:
            self.sendAmountTextfield.text = [NSString stringWithFormat:@"%d", kDefaultAmount];
            [self updateReceivedAmount];
            break;
        case POPUP_TYPE_CURRENCIES_ERROR:
            [self fetchCurrencies];
            break;
        case POPUP_TYPE_PAY_ERROR:
            break;
    }
}

@end
