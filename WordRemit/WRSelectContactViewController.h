//
//  WRSelectContactViewController.h
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  View controller used to select a recipient for the payment.
 */
@interface WRSelectContactViewController : UITableViewController

@end
