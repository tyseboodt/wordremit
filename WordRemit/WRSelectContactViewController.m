//
//  WRSelectContactViewController.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import "WRSelectContactViewController.h"
#import "WRPaymentViewController.h"

@interface WRSelectContactViewController ()

@property (strong, nonatomic) NSArray *contacts;
@property (copy, nonatomic) NSString *recipientName;

@end

@implementation WRSelectContactViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Select Contact", nil);
    self.contacts = @[@"test", @"test2", @"test3"];
    [self.tableView reloadData];
    
    // Do any additional setup after loading the view.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell.textLabel setText:self.contacts[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *currentCell = [self.tableView cellForRowAtIndexPath:indexPath];
    self.recipientName = currentCell.textLabel.text;
    [self performSegueWithIdentifier:@"WRSelectContactViewController" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"WRSelectContactViewController"] &&
        [segue.destinationViewController isKindOfClass:[WRPaymentViewController class]]) {
        WRPaymentViewController *paymentViewController = (WRPaymentViewController *)segue.destinationViewController;
        [paymentViewController injectRecipientName:self.recipientName];
    }
}

@end
