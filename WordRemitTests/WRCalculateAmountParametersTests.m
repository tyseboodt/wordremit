//
//  WRCalculateAmountParametersTests.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 29/06/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "WRCalculateAmountParameters.h"

@interface WRCalculateAmountParametersTests : XCTestCase

@end

@implementation WRCalculateAmountParametersTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValidJSONDictionary {
    // This is an example of a functional test case.
    WRCalculateAmountParameters *parameters = [[WRCalculateAmountParameters alloc] initWithAmount:@120
                                                                                     sendCurrency:@"GBP"
                                                                                  receiveCurrency:@"EUR"];
    NSDictionary *dictionary = [MTLJSONAdapter JSONDictionaryFromModel:parameters
                                                                        error:nil];
    XCTAssert(dictionary != nil &&
              [dictionary[@"amount"] isEqual:@120] &&
              [dictionary[@"sendcurrency"] isEqualToString:@"GBP"] &&
              [dictionary[@"receivecurrency"] isEqualToString:@"EUR"], @"dictionary should be valid");
}
@end
