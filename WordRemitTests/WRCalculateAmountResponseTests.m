//
//  WRCalculateAmountResponseTests.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 29/06/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "WRCalculateAmountResponse.h"

@interface WRCalculateAmountResponseTests : XCTestCase

@end

@implementation WRCalculateAmountResponseTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValidJSONDictionary {
    // This is an example of a functional test case.
    NSDictionary *json = @{
                           @"sendamount": @120,
                           @"sendcurrency": @"GBP",
                           @"receiveamount": @(105.12),
                           @"receivecurrency": @"EUR",
                           @"recipient": @"Bob Classy"
                           };
    WRCalculateAmountResponse *response = [MTLJSONAdapter modelOfClass:[WRCalculateAmountResponse class]
                                                    fromJSONDictionary:json error:nil];
    XCTAssert(response != nil &&
              [response.sendAmount isEqual:@120] &&
              [response.sendCurrency isEqualToString:@"GBP"] &&
              [response.receiveAmount isEqual:@(105.12)] &&
              [response.receiveCurrency isEqualToString:@"EUR"], @"WRCalculateAmountResponse should be valid");
}

@end
