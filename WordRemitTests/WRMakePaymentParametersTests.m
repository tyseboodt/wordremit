//
//  WRMakePaymentParametersTests.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 29/06/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "WRMakePaymentParameters.h"

@interface WRMakePaymentParametersTests : XCTestCase

@end

@implementation WRMakePaymentParametersTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValidJSONDictionary {
    // This is an example of a functional test case.
    WRMakePaymentParameters *parameters = [[WRMakePaymentParameters alloc] initWithSendAmount:@120
                                                                                 sendCurrency:@"GBP"
                                                                                receiveAmount:@150
                                                                              receiveCurrency:@"EUR"
                                                                                    recipient:@"Bob"];
    NSDictionary *dictionary = [MTLJSONAdapter JSONDictionaryFromModel:parameters
                                                                 error:nil];
    XCTAssert(dictionary != nil &&
              [dictionary[@"sendamount"] isEqual:@120] &&
              [dictionary[@"sendcurrency"] isEqualToString:@"GBP"] &&
              [dictionary[@"receiveamount"] isEqual:@150] &&
              [dictionary[@"receivecurrency"] isEqualToString:@"EUR"] &&
              [dictionary[@"recipient"] isEqualToString:@"Bob"], @"dictionary should be valid");
}

@end
