//
//  WRNetworkManagerTests.m
//  WordRemit
//
//  Created by Thierry Yseboodt on 17/05/2015.
//  Copyright (c) 2015 Thierry Yseboodt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "WRNetworkManager.h"
#import "Nocilla.h"
#import "WRCalculateAmountParameters.h"
#import "WRCalculateAmountResponse.h"
#import "WRMakePaymentParameters.h"

@interface WRNetworkManagerTests : XCTestCase

@end

@implementation WRNetworkManagerTests

- (void)setUp {
    [super setUp];
    [[LSNocilla sharedInstance] start];
}

- (void)tearDown {
    [[LSNocilla sharedInstance] stop];
    [[LSNocilla sharedInstance] clearStubs];
    [super tearDown];
}

- (void)testValidCurrenciesJSONReturned {
    __block BOOL done = NO;
    stubRequest(@"GET", @"https://wr-interview.herokuapp.com/api/currencies").andReturn(200).withHeaders(@{@"Content-Type": @"application/json"}).withBody(@"[\"GBP\", \"USD\", \"PHP\", \"EUR\"]");
    [WRNetworkManager currencies:^(NSArray *currencies) {
        XCTAssert(currencies != nil && currencies.count == 4, @"currencies should be a valid array");
        done = YES;
    }];
    NSUInteger timeout = 0;
    while (done == NO && timeout < 30) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        timeout++;
    }
    
    XCTAssertTrue(timeout < 30, @"Currencies data is not available");
}


- (void)testInvalidCurrenciesJSONReturned {
    __block BOOL done = NO;
    stubRequest(@"GET", @"https://wr-interview.herokuapp.com/api/currencies").andReturn(200).withHeaders(@{@"Content-Type": @"application/json"}).withBody(@"[\"GBP\" \"USD\", \"PHP\", \"EUR\"]");
    [WRNetworkManager currencies:^(NSArray *currencies) {
        XCTAssert(currencies == nil, @"currencies should be nil");
        done = YES;
    }];
    NSUInteger timeout = 0;
    while (done == NO && timeout < 30) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        timeout++;
    }
    
    XCTAssertTrue(timeout < 30, @"Currencies data is not available");
    
}


- (void)testCalculateValidJSONReturned {
    __block BOOL done = NO;
    stubRequest(@"GET", @"https://wr-interview.herokuapp.com/api/calculate".regex).andReturn(200).withHeaders(@{@"Content-Type": @"application/json"}).withBody(@"{\"sendamount\": 120, \"sendcurrency\": \"GBP\",\"receiveamount\": 105.12,\"receivecurrency\": \"EUR\",\"recipient\": \"Bob Classy\"}");
    
    WRCalculateAmountParameters *params = [[WRCalculateAmountParameters alloc] initWithAmount:@(120)
                                                                                 sendCurrency:@"GBP"
                                                                              receiveCurrency:@"EUR"];
    [WRNetworkManager calculateAmount:params
                      completionBlock:^(WRCalculateAmountResponse *response) {
                          XCTAssert(response != nil && [response.receiveAmount isEqual:@(105.12)], @"Response should not be nil and receive amount should be 105.12");
                          done = YES;
                      }];
    NSUInteger timeout = 0;
    while (done == NO && timeout < 30) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        timeout++;
    }
    
    XCTAssertTrue(timeout < 30, @"Calculate data is not available");
    
}


- (void)testCalculateInvalidJSONReturned {
    __block BOOL done = NO;
    stubRequest(@"GET", @"https://wr-interview.herokuapp.com/api/calculate".regex).andReturn(200).withHeaders(@{@"Content-Type": @"application/json"}).withBody(@"{\"sendamou: 120, \"sendcurrency\": \"GBP\",\"receiveamount\": 105.12,\"receivecurrency\": \"EUR\",\"recipient\": \"Bob Classy\"}");
    
    WRCalculateAmountParameters *params = [WRCalculateAmountParameters new];
    [WRNetworkManager calculateAmount:params
                      completionBlock:^(WRCalculateAmountResponse *response) {
                          XCTAssert(response == nil, @"Response should be nil if invalid json is returned");
                          done = YES;
                      }];
    NSUInteger timeout = 0;
    while (done == NO && timeout < 30) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        timeout++;
    }
    
    XCTAssertTrue(timeout < 30, @"Calculate data is not available");
    
}

- (void)testValidPaymentResponse {
    __block BOOL done = NO;
    stubRequest(@"POST", @"https://wr-interview.herokuapp.com/api/send").andReturn(201);
    WRMakePaymentParameters *params = [WRMakePaymentParameters new];
    [WRNetworkManager pay:params completionBlock:^(BOOL completion) {
        XCTAssert(completion, @"completion should be true when 201 is returned");
        done = YES;
    }];
    NSUInteger timeout = 0;
    while (done == NO && timeout < 30) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        timeout++;
    }
    
    XCTAssertTrue(timeout < 30, @"Payment data is not available");

}

- (void)testValidPaymentInvalidResponse {
    __block BOOL done = NO;
    stubRequest(@"POST", @"https://wr-interview.herokuapp.com/api/send").andReturn(400);
    WRMakePaymentParameters *params = [WRMakePaymentParameters new];
    [WRNetworkManager pay:params completionBlock:^(BOOL completion) {
        XCTAssert(completion == NO, @"completion should be false when another code than 201 is returned");
        done = YES;
    }];
    NSUInteger timeout = 0;
    while (done == NO && timeout < 30) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        timeout++;
    }
    
    XCTAssertTrue(timeout < 30, @"Payment data is not available");

}



@end
